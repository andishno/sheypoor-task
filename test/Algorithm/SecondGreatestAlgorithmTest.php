<?php

namespace Test\Algorithm;


use App\Algorithm\SecondGreatestAlgorithm;
use PHPUnit\Framework\TestCase;


/**
 * Class SecondGreatestAlgorithmTest
 *
 * @package Test\Algorithm
 */
class SecondGreatestAlgorithmTest extends TestCase
{
    public function testConstructor()
    {
        $this->expectNotToPerformAssertions();

        new SecondGreatestAlgorithm([1, 2, 3, 4, 5, 6]);
    }

    /**
     * @test SecondGreatestAlgorithm::find()
     */
    public function testFind()
    {
        $algObj = new SecondGreatestAlgorithm([3, 1, 3, 4, 5, 6]);
        $secondGreatest = $algObj->find();
        $this->assertSame(5, $secondGreatest);
    }
}
