<?php

namespace Test\Algorithm;


use App\Algorithm\NthGreatestAlgorithm;
use PHPUnit\Framework\TestCase;


/**
 * Class NthGreatestAlgorithmTest
 *
 * @package Test\Algorithm
 */
class NthGreatestAlgorithmTest extends TestCase
{
    public function testConstructor()
    {
        $this->expectNotToPerformAssertions();

        new NthGreatestAlgorithm([1, 2, 3, 4, 5, 6]);
    }

    /**
     * @test NthGreatestAlgorithm::find()
     */
    public function testFind()
    {
        $algObj = new NthGreatestAlgorithm([1, 2, 3, 4, 5, 6]);
        $thirdGreatest = $algObj->find(3);
        $this->assertSame(4, $thirdGreatest);
    }
}
