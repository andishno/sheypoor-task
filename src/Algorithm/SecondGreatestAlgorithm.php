<?php

namespace App\Algorithm;


use Exception;
use InvalidArgumentException;

/**
 * Class SecondGreatestAlgorithm
 *
 * This class finds the second greatest element of an arbitrarily
 * ordered array of elements.
 *
 * @package App\Algorithm
 */
class SecondGreatestAlgorithm
{
    /** @var array */
    private $data;

    /**
     * SecondGreatestAlgorithm constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Finds the second greatest element of the data.
     * The part with most complexity now is only the foreach loop so the complexity for the function
     * must come to O(n).
     *
     * @return mixed
     */
    public function find()
    {
        try {
            if (count($this->data) < 2){
                throw new InvalidArgumentException("Array must have at least 2 elements");
            }
            $greatest = $second = null;
            foreach ($this->data as $datum) {
                if ($datum > $greatest) {
                    $second = $greatest;
                    $greatest = $datum;
                } else if ($datum > $second && $datum != $greatest) {
                    $second = $datum;
                }
            }
            return (int) $second;
        }catch (Exception $exception){
            return $exception;
        }
    }
}
