<?php

namespace App\Algorithm;


use Exception;
use InvalidArgumentException;

/**
 * Class NthGreatestAlgorithm
 *
 * This class finds the nth greatest element of an arbitrarily
 * ordered array of elements.
 *
 * @package App\Algorithm
 */
class NthGreatestAlgorithm
{
    /** @var array */
    private $data;

    /**
     * NthGreatestAlgorithm constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Finds the nth greatest element of the data.
     * This one uses the QuickSort to sort the array then pick the nth greatest element so
     * the complexity is the complexity of the sort function which is at worst, O(n^2).
     *
     * @return mixed
     */
    public function find($n)
    {
        try {
            if (count($this->data) < $n){
                throw new InvalidArgumentException("Array must have at least $n elements");
            }
            $sorted = $this->sort($this->data);
            return $sorted[$n - 1];
        }catch (Exception $exception){
            return $exception;
        }
    }

    /**
     * Sorting function using QuickSort
     * @param $array
     * @return array
     */
    private function sort($array)
    {
        $length = count($array);
        if (!$length) {
            return $array;
        }

        $k = $array[0];
        $x = $y = [];

        for ($i = 1; $i < $length; $i++) {
            if ($array[$i] >= $k) {
                $x[] = $array[$i];
            } else {
                $y[] = $array[$i];
            }
        }
        return array_merge($this->sort($x), array($k), $this->sort($y));
    }
}
